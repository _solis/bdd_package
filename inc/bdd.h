/**
 * @ingroup bddLib
 * @brief Includes all headers for the BDD package
 * @file bdd.h
 * @author Mauricio Solis
*/
#ifndef __BDD_HEADER_H__
#define __BDD_HEADER_H__
// All files to include
#include <bddVersions.h>
#include <bddManagerInterface.h>
#include <bddManager.h>
#endif /* __BDD_HEADER_H__ */
