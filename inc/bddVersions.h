/*
 *  @file    bddVersions.h
 *  @brief   Report the version for this package.
 *  @author  Mauricio Solis
*/
#ifndef __VERSIONS_H__
#define __VERSIONS_H__

/// The current code versions
#define PKG_VERSION 1.0.0
#define TST_VERSION 1.1.0

#endif /* __VERSIONS_H__ */
