/**
 * @ingroup bddLib
 * @brief Implementaion of all Interface's class methods
 * @file bddManager.h
 * @author Mauricio Solis
*/

/// Header
#include <bddManager.h>

/// STL
#include <unordered_map>
#include <iostream>
/// Add specified namespace
namespace bdd {
    // Define static vars
    const BDD_ID Manager::trueId;
    const BDD_ID Manager::falseId;
    const BDD_ID Manager::MANAGER_FAIL;
    /// Constructor
    Manager::Manager(void)
    {   
        uniqueTable 	= uniqueTable_t();
        computeTable 	= computeTable_t();
        lookUpTable     = lookUpTable_t();
        // Insert 0 and configure
        createVar("0");
        (uniqueTable.at(0))->highId = falseId;
        // Insert 1 and configure
        createVar("1");
        (uniqueTable.at(1))->lowId = trueId;
    }
    /// Destructor
    Manager::~Manager(void) {}
    
    /* -------------- Private Methods -------------- */
    /// Checks validity of variables without searching tables
    const bool Manager::validId(BDD_ID i, BDD_ID t, BDD_ID e)
    {
        //check for corrupted input
        if (i >= currentId || i < 0)
        {
            return false;
        }
        if (t >= currentId || t < 0)
        {
            return false;
        }
        if (e >= currentId || e < 0)
        {
            return false;
        }
        return true;
    }
    /// Inserts into unique table and returns it's ID
    const BDD_ID Manager::insertUniqueTable(    const BDD_ID &highId, const BDD_ID &lowId, 
                                                const BDD_ID &topVar, const std::string &label)
    {
        // Create node and insert
        Node *n = new Node(currentId, highId, lowId, topVar, label);
        BDD_ID exists = searchLookupTable(label);
        // If it doesn't exist, insert
        if(exists == MANAGER_FAIL)
        {
            // Create new key and insert to tables
            uniqueTable.push_back(n);
            lookUpTable.insert({label, currentId});
            // We don't want constants or variables in compute table, unless they are negations
            if( (!isConstant(currentId) && !isVariable(currentId)) || negation) 
            {
                ITE_ID key(topVar, highId, lowId);
                computeTable.insert({key, currentId});
                negation = false;
            }
            return currentId++;
        }
        // If it does exist, simply return the BDD_ID from previous search
        return exists;
    }
    /// Searche look up table for label and returns the associated ID
    const BDD_ID Manager::searchLookupTable(const std::string &label)
    {
        lIter_t it = lookUpTable.find(label);
        // If the item exists
        if(it != lookUpTable.end()) 
        {
            return it->second;
        }
        return MANAGER_FAIL;
    }
    /// Searches compute table for a given ITE_ID and returns the BDD_ID with corresponding value
    const BDD_ID Manager::searchComputeTable(const ITE_ID &id)
    {
        cIter_t it = computeTable.find(id);
        // If the item exists
        if(it != computeTable.end()) 
        {
            return it->second;
        }
        return MANAGER_FAIL;
    }
    /* -------------- Public Methods -------------- */
    /**
    * True
    *
    * @return ID for the node representing "true"
    */
    const BDD_ID &Manager::True(void)
    {
        return trueId;
    }
    /**
    * False
    *
    * @return ID for the node representing "false"
    */
    const BDD_ID &Manager::False(void)
    {
        return falseId;
    }
    /**
    * isConstant
    * @brief    Define a terminal node if it is either "0" or "1"
    *
    * @param x BDD_ID that should be checked
    *
    * @return True if x is a leaf/terminal node
    */
    const bool Manager::isConstant(const BDD_ID &x)
    {
        return ((x == trueId) || (x == falseId));
    }
    /**
    * isVariable
    * @brief    Define a varaible as not being a constant and it's not a function
    *           x(high == True or False) and x(low == True or False)
    *
    * @param x BDD_ID that should be checked
    *
    * @return True if x is a variable/non-terminal node
    */
    const bool Manager::isVariable(const BDD_ID &x)
    {
        if(validId(x))
        {
            Node *it = uniqueTable.at(x);
            if(isConstant(it->myId)) 
            {
                return false;   // It's a constant
            }
            // Check positive/negative coFactors and verify it's either 0 or 1
            if( (it->highId == trueId) && (it->lowId == falseId) )
            {
                return true;
            }
        }
        return false;
    }
    /**
    * createVar
    *
    * @param label The label that will be used to create the node and insert to the table
    *
    * @return BDD_ID if it is a new variable, otherewise, BDD_ID of existing node
    */
    const BDD_ID Manager::createVar(const std::string &label)
    {
        return insertUniqueTable(True(), False(), currentId, label);
    }
    /**
    * uniqueTableSize
    *
    * @return Number of the nodes currently in the unique table
    */
    const std::size_t Manager::uniqueTableSize(void)
    {
       return uniqueTable.size();
    }
    /**
    * topVar
    *
    * @param f ID of the node for top variable request
    *
    * @return BDD_ID of top variable of node f
    */
    const BDD_ID Manager::topVar(const BDD_ID &f)
    {
        if(validId(f))
        {
            return uniqueTable.at(f)->topVar;
        }
        return MANAGER_FAIL;
    }
    /**
    * getTopVarName
    *
    * @param f ID of the node for label request of its top variable
    *
    * @return Label of top variable of node f
    */
    const std::string Manager::getTopVarName(const BDD_ID &f)
    {
        if(validId(f))
        {
            Node *it = uniqueTable.at(f);
            return uniqueTable.at(it->topVar)->label;
        }
        return std::to_string(MANAGER_FAIL);
    }
    /**
    * ite
    *
    * @param i ID of the node for the "if" node for the if-then-else operation
    * @param t ID of the node for the "then" node for the if-then-else operation
    * @param e ID of the node for the "else" node for the if-then-else operation
    *
    * @return ID representation of the ITE operation
    */
    const BDD_ID Manager::ite(const BDD_ID &i, const BDD_ID &t, const BDD_ID &e)
    {
        // Check input validity
        if(validId(i, t, e) == false)
        {
            return MANAGER_FAIL;
        }
        // Terminal cases:
        if(i == trueId || t == e)
        {
            return t;
        }
        else if(i == falseId)
        {
            return e;
        }
        else if( (t == falseId) && (e == trueId) )
        {
            negation = true;
        }
        ITE_ID id(i, t, e);
        // Check compute table
        BDD_ID exists = searchComputeTable(id);
        if(exists != MANAGER_FAIL) 
        {
            // Already exists
            return exists;
        }
        // Do the arithmetic
        else
        {
            // Let v be top MOST varaible of {i, t, e}
            BDD_ID topMostVar   = currentId + 1;
            BDD_ID topVar_i     = topVar(i);  // By this point, we know !isConstant(i)
            BDD_ID topVar_t     = isConstant(t) ? MANAGER_FAIL : topVar(t);
            BDD_ID topVar_e     = isConstant(e) ? MANAGER_FAIL : topVar(e);
            
            // Determine top most varaible
            topMostVar = topVar_i;
            if( (topVar_t != MANAGER_FAIL) && (topVar_t < topMostVar) )
            {
                topMostVar = topVar_t;
            }
            if( (topVar_e != MANAGER_FAIL) && (topVar_e < topMostVar) )
            {
                topMostVar = topVar_e;
            }
            // Now coFactor
            BDD_ID coHi = ite(coFactorTrue(i, topMostVar), coFactorTrue(t, topMostVar), coFactorTrue(e, topMostVar));
            BDD_ID coLo = ite(coFactorFalse(i, topMostVar), coFactorFalse(t, topMostVar), coFactorFalse(e, topMostVar));
            // Check for reduction - a new internal node isn't needed 
            // if coHi == coLo, then: remove this node and redirect all edges to this node's sucessor
            if(coHi == coLo)
            {
                return coHi;
            }
            // Check if sucessors are equal to top most variable's sucessors
            else if( (coHi == coFactorTrue(topMostVar)) && (coLo == coFactorFalse(topMostVar)) )
            {
                return topMostVar;
            }
            // Re-check if this ite(topMostVar, coHi, coLo) already exists
            id = {topMostVar, coHi, coLo};
            exists = searchComputeTable(id);
            if(exists != MANAGER_FAIL)
            {
                // Return already existing node - no need to add a new one
                return exists;
            }
            else
            {
                // Create new node and insert to tables
                std::string label = \
                "ite(" + std::to_string(i) + ", " + std::to_string(t) + ", " + std::to_string(e) + ")";
                return insertUniqueTable(coHi, coLo, topMostVar, label);
            }
        }
        // Something went wrong?  This should never happen
        return MANAGER_FAIL;
    }
    /**
    * coFactorFalse
    *
    * @param f ID of the node for a function 'f'
    *
    * @return ID representation of the negative cofactor defined by the function 'f'
    */
    const BDD_ID Manager::coFactorFalse(const BDD_ID &f)
    {
        if(validId(f))
        {
            return uniqueTable.at(f)->lowId;
        }
        return MANAGER_FAIL;
    }      
    /**
    * coFactorTrue
    *
    * @param f ID of the node for a function 'f'
    *
    * @return ID representation of the positive cofactor defined by the function 'f'
    */        
    const BDD_ID Manager::coFactorTrue(const BDD_ID &f)
    {
        if(validId(f))
        {
            return uniqueTable.at(f)->highId;
        }
        return MANAGER_FAIL;
    }
    /**
    * coFactorFalse
    *
    * @param f ID of the node for a function 'f'
    * @param x ID of the node for a variable in function 'f'
    *
    * @return ID representation of the negative cofactor for f(x), x = 0
    */
    const BDD_ID Manager::coFactorFalse(const BDD_ID &f, BDD_ID &x)
    {
        if(!validId(f, x))
        {
            return MANAGER_FAIL;
        }
        // So we don't run several searches, let's do it once
        Node *it_f    = uniqueTable.at(f);
        Node *it_x    = uniqueTable.at(x);
        // If f is a constant, return whatever it is
        if(isConstant(f)) 
        {
            return f;
        }
        else
        {
            // check if x is not a variable
            if(!isConstant(x) && (!isConstant(it_x->highId) || !isConstant(it_x->lowId)) )
            {
                x = it_x->topVar;
            }
            // Check variable order
            if(it_f->topVar > x)
            {
                // x has no affect on f
                return it_f->myId;
            }
            // x == f's topVar, return f's highId
            else if(x == it_f->topVar)
            {
                return it_f->lowId;
            }
            // x is further down the BDD and we need to iterate to find it
            else
            {
                BDD_ID coHi = coFactorFalse(it_f->highId, x);
                BDD_ID coLo = coFactorFalse(it_f->lowId, x);
                // Reroute to here if iso-graphs
                if(coHi == coLo)
                {
                    return coHi;
                }
                // Check if sucessors are equal to top variable's sucessors
                else if(    (coHi == coFactorTrue(it_f->topVar)) && \
                            (coLo == coFactorFalse(it_f->topVar)) )
                {
                    return it_f->topVar;
                }
                else
                {
                    ITE_ID id = {it_f->topVar, coHi, coLo};
                    BDD_ID exists = searchComputeTable(id);
                    if (exists != MANAGER_FAIL)
                    {
                        return exists;
                    }
                    // Insert new node
                    std::string label = \
                    "coFactorFalse(" + std::to_string(it_f->myId) + ", " + it_x->label + ")";
                    return insertUniqueTable(coHi, coLo, it_f->topVar, label);
                }
            }
        } 
        return MANAGER_FAIL;
    }
    /**
    * coFactorTrue
    *
    * @param f ID of the node for a function 'f'
    * @param x ID of the node for a variable in function 'f'
    *
    * @return ID representation of the positive cofactor for f(x), x = 1
    */
    const BDD_ID Manager::coFactorTrue(const BDD_ID &f, BDD_ID &x)
    {
        if(!validId(f, x))
        {
            return MANAGER_FAIL;
        }
        Node *it_f    = uniqueTable.at(f);
        Node *it_x    = uniqueTable.at(x);
        // If f is a constant, return whatever it is
        if(isConstant(f)) 
        {
            return f;
        }
        else
        {
            // check if x is not a variable
            if(!isConstant(x) && (!isConstant(it_x->highId) || !isConstant(it_x->lowId)) )
            {
                x = it_x->topVar;
            }
            // Check variable order
            if(it_f->topVar > x)
            {
                // x has no affect on f
                return it_f->myId;
            }
            // x == f's topVar, return f's highId
            else if(x == it_f->topVar)
            {
                return it_f->highId;
            }
            // x is further down the BDD and we need to iterate to find it
            else
            {
                BDD_ID coHi = coFactorTrue(it_f->highId, x);
                BDD_ID coLo = coFactorTrue(it_f->lowId, x);
                // Reroute to here if iso-graphs
                if(coHi == coLo)
                {
                    return coHi;
                }
                // Check if sucessors are equal to top variable's sucessors
                else if(    (coHi == coFactorTrue(it_f->topVar)) && \
                            (coLo == coFactorFalse(it_f->topVar)) )
                {
                    return it_f->topVar;
                }
                else
                {
                    ITE_ID id = {it_f->topVar, coHi, coLo};
                    BDD_ID exists = searchComputeTable(id);
                    if (exists != MANAGER_FAIL)
                    {
                        return exists;
                    }
                    // Insert new node
                    std::string label = \
                    "coFactorTrue(" + std::to_string(it_f->myId) + ", " + it_x->label + ")";
                    return insertUniqueTable(coHi, coLo, it_f->topVar, label);
                }
            }
        }
        return MANAGER_FAIL;
    }
    /**
    * findNodes
    *
    * @param root ID for 'starting' point of sub-graph request
    * @param nodesOfRoot Set where all reachable nodes from root, including root, are saved
    *
    * @return ID None
    */
    void Manager::findNodes(const BDD_ID &root, std::set<BDD_ID> &nodesOfRoot)
    {
        if (!validId(root))
        {
            return;
        }
        // Insert root node
        nodesOfRoot.insert(root);
        if (isConstant(root))
        {
            return;
        }
        else
        {
            Node *rootNode      = uniqueTable.at(root);
            // Recursivley insert all high and low nodes from root
            BDD_ID highOfRoot   = rootNode->highId;
            BDD_ID lowOfRoot    = rootNode->lowId;
            findNodes(highOfRoot, nodesOfRoot);
            findNodes(lowOfRoot, nodesOfRoot);
        }
        return;
    }
    /**
    * findVars
    *
    * @param root ID for 'starting' point of top variable sub-graph request
    * @param varsOfRoot Set where all reachable nodes' top varaibles from root, including root, are saved
    *
    * @return ID None
    */
    void Manager::findVars(const BDD_ID &root, std::set<BDD_ID> &varsOfRoot)
    {
        if(!validId(root))
        {
            return;
        }
        if(isConstant(root))
        {
            varsOfRoot.insert(root);
            return;
        }
        else
        {
            Node *rootNode      = uniqueTable.at(root);
            varsOfRoot.insert(rootNode->topVar);
            BDD_ID highOfRoot   = rootNode->highId;
            BDD_ID lowOfRoot    = rootNode->lowId;
            findVars(highOfRoot, varsOfRoot);
            findVars(lowOfRoot, varsOfRoot);
        }
    }
    /**
    * neg
    *
    * @param a ID of node to be inverted
    *
    * @return ID of 'a' negation
    */
    const BDD_ID Manager::neg(const BDD_ID &a)
    {
       return ite(a, 0, 1);
    }
    /**
    * and2
    *
    * @param a ID of first operand
    * @param b ID of second operand
    *
    * @return ID of 'a' AND 'b' operation
    */
    const BDD_ID Manager::and2(const BDD_ID &a, const BDD_ID &b)
    {
       return ite(a, b, 0);
    }
    /**
    * nand2
    *
    * @param a ID of first operand
    * @param b ID of second operand
    *
    * @return ID of 'a' NAND 'b' operation
    */
    const BDD_ID Manager::nand2(const BDD_ID &a, const BDD_ID &b)
    {
       return ite(a, neg(b), 1);;
    }
    /**
    * or2
    *
    * @param a ID of first operand
    * @param b ID of second operand
    *
    * @return ID of 'a' OR 'b' operation
    */
    const BDD_ID Manager::or2(const BDD_ID &a, const BDD_ID &b)
    {
       return ite(a, 1, b);
    }
    /**
    * xor2
    *
    * @param a ID of first operand
    * @param b ID of second operand
    *
    * @return ID of 'a' XOR 'b' operation
    */
    const BDD_ID Manager::xor2(const BDD_ID &a, const BDD_ID &b)
    {
       return ite(a, neg(b), b);
    }
    /**
    * nor2
    *
    * @param a ID of first operand
    * @param b ID of second operand
    *
    * @return ID of 'a' NOR 'b' operation
    */
    const BDD_ID Manager::nor2(const BDD_ID &a, const BDD_ID &b)
    {
       return ite(a, 0, neg(b));
    }
    /**
    * printTables
    *
    * @brief Prints all tables
    */
    void Manager::printTables(void)
    {
        // Unique table
        std::cout << " --- UNIQUE TABLE --- " << std::endl;
        for(auto it = uniqueTable.begin(); it != uniqueTable.end(); ++it)
        {
            std::cout << *it << std::endl;
        }  
        // Compute table
        std::cout << " --- COMPUTE TABLE --- " << std::endl;
        for(auto it = computeTable.begin(); it != computeTable.end(); ++it)
        {
            std::cout << "BDD_ID[" << it->second << "]: " << it->first << std::endl;
        }
        std::cout << " --- LOOKUP TABLE --- " << std::endl;
        for(auto it = lookUpTable.begin(); it != lookUpTable.end(); ++it)
        {
            std::cout << "Label[" << it->first << "], BDD_ID[" << it->second << "]" << std::endl;
        }
    }
} // namespace bdd
