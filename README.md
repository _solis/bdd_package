## DESCRIPTION
This project implements a minimal Binary Decision Diagram (BDD) package in C++. This package shall implement the fundamental manipulation methods for Reduced Ordered BDDs (ROBDDs) as they were introduced in the lecture Verification of Digital System by Prof. Kunz at the Technische Universität Kaiserslautern. The package will be implemented using the Test Driven Development pardigm presented by Dr. Wedler.

###### Part I: Develop a BDD Package following a TDD (test driven development) approach
- Write the tests for the BDD package
- Implement the package in a C++ class
- Run the code against the predefined tests
    
## SETUP
GoogleTest is required for compile. Running CMake will check for the gtest and gtest_main packages, if not found; it will note build the test suite.

## BUILD
```
cd build/  
cmake CMakeLists.txt  
make all  
```
OR  
```
make bddPkg  
make bddPkgMain (bddPkg dependent)  
make bddTestSuite (bddPkg & googletest dependent)  
```

## EXECUTE
From *build/*, executables are located in *bin/* directory  
```
./bddPkgMain.out   
./bddTestSuite.out
```
