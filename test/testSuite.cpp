/**
 * @ingroup bddTestSuite
 * @brief Main function for test suite
 * @file testSuite.cpp
 * @author Mauricio Solis
*/
/// Tests
#include "testSuite.h"

/// main
int main(int argc, char **argv)
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
