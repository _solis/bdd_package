/**
 * @ingroup bddTestSuite
 * @brief Source for application
 * @file main.cpp
 * @author Mauricio Solis
*/
#include <iostream>
#include <bdd.h>

/// main
using namespace bdd;
using namespace std;

int main ()
{
    manager_t m;
    

    // insert new VARIABLE nodes - TRUE
    BDD_ID a = m.createVar("a");
    BDD_ID b = m.createVar("b");

    // Should return true since we're creating a new variable with negatation
    BDD_ID c = m.isVariable(m.neg(m.createVar("b")));
    m.printTables();
    cout<<"c: " << c << endl;
    return 0;
}
