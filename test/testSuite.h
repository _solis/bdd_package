/**
 * @ingroup bddTestSuite
 * @brief All BDD package tests
 * @file testSuite.h
 * @author Mauricio Solis
*/
#ifndef __TEST_SUITE_H__
#define __TEST_SUITE_H__
/// GoogleTest
#include <gtest/gtest.h>
/// Library header
#include <bdd.h>
/// Declare namespace
using namespace bdd;
/** 
* \brief Test for trueId = 1
*/
TEST(bdd, True)
{
    manager_t m;
    ASSERT_EQ(1, m.True());
}
/**
* \brief Test falseId = 0
*/
TEST(bdd, False)
{
    manager_t m;
    ASSERT_EQ(0, m.False());
}
/**
* \brief Should return true if provided a leaf node, false and true nodes are terminal by default
*/
TEST(bdd, isConstant)
{
    manager_t m;
    ASSERT_TRUE(m.isConstant(0));
    ASSERT_TRUE(m.isConstant(1));
    ASSERT_FALSE(m.isConstant(2));
    ASSERT_FALSE(m.isConstant(-1));
}
/**
* \brief Should return true if provided a variable BDD_ID
*/
TEST(bdd, isVariable)
{
    manager_t m;
    // check when constants 0 and 1 are given - FALSE
    ASSERT_FALSE(m.isVariable(0));
    ASSERT_FALSE(m.isVariable(1));

    // insert new VARIABLE nodes - TRUE
    BDD_ID a = m.createVar("a");
    BDD_ID b = m.createVar("b");
    ASSERT_TRUE(m.isVariable(a));
    ASSERT_TRUE(m.isVariable(b));

    // insert a new that is NOT a variable - FALSE
    BDD_ID newVar = m.and2(a, b);
    ASSERT_FALSE(m.isVariable(newVar));
    // Should return true since we're creating a new variable with negatation
    ASSERT_TRUE(!m.isVariable(m.neg(m.createVar("b"))) );
    
    //  non-existent variable
    ASSERT_FALSE(m.isVariable(9000));
    ASSERT_FALSE(m.isVariable(-4));
}
/**
* \brief Test for createVar from label, return BDD_ID, 0 and 1 are false and true BDD_IDs (taken BDD_IDs), so next should be 2, 3, 4, etc...
*/
TEST(bdd, createVar)
{
    manager_t m;
    ASSERT_EQ(2, m.createVar("varA"));
    ASSERT_EQ(3, m.createVar("varB"));
    ASSERT_EQ(4, m.createVar("varC"));
    // Should return the same ID
    ASSERT_EQ(4, m.createVar("varC"));
    ASSERT_EQ(3, m.createVar("varB"));
    ASSERT_EQ(2, m.createVar("varA"));
}
/**
* \brief Test for uniqueTableSize()
*/
TEST(bdd, uniqueTableSize)
{
    manager_t m;
    // initial table size should be 2 due to the false and true nodes.
    ASSERT_EQ(2, m.uniqueTableSize());
    // insert something, size of 5 after this
    m.createVar("varA");  // 2
    m.createVar("varB");  // 3
    m.createVar("varC");  // 4
    ASSERT_EQ(5, m.uniqueTableSize());
}
/**
* \brief Test for topVar, should return the top variable for a given BDD_ID
*/
TEST(bdd, topVar)
{
    manager_t m;
    ASSERT_EQ(0, m.topVar(0));
    ASSERT_EQ(1, m.topVar(1));
    
    // Insert variables
    BDD_ID a = m.createVar("a");
    BDD_ID b = m.createVar("b");
    BDD_ID c = m.createVar("c");
    BDD_ID d = m.createVar("d");
    // f = (a + b) * (c * d)
    BDD_ID ab_or = m.or2(a, b);
    BDD_ID cd_and = m.and2(c, d);

    BDD_ID f = m.and2(ab_or, cd_and);
    ASSERT_EQ(a, m.topVar(f));
    
    // This shouldn't exist
    ASSERT_EQ(m.MANAGER_FAIL, m.topVar(100));
}
/**
* \brief Should return the top variable label for a given BDD_ID
*/
TEST(bdd, getTopVarName)
{
    manager_t m;

    BDD_ID a = m.createVar("a");
    BDD_ID b = m.createVar("b");
    BDD_ID c = m.createVar("c");
    
    // Should fail
    ASSERT_EQ(std::to_string(m.MANAGER_FAIL), m.getTopVarName(-2));
    ASSERT_EQ(std::to_string(m.MANAGER_FAIL), m.getTopVarName(100));
    
    // Should pass
    ASSERT_EQ("a", m.getTopVarName(a));
    ASSERT_EQ("b", m.getTopVarName(b));
    ASSERT_EQ("c", m.getTopVarName(c));
}
/**
* \brief Test for ite terminal cases
*/
TEST(bdd, ite_Terminal)
{
    manager_t m;

    BDD_ID a = m.createVar("a");
    BDD_ID b = m.createVar("b");
    BDD_ID c = m.createVar("c");

    // ite(1, c, b) = ite(0, b, c) = ite(c, 1, 0) = c
    ASSERT_EQ(c, m.ite(1, c, b));
    ASSERT_EQ(c, m.ite(0, b, c));
    ASSERT_EQ(c, m.ite(c, 1, 0));
    ASSERT_EQ(m.neg(c), m.ite(c, 0, 1));
    ASSERT_EQ(c, m.ite(a, c, c));
    
    // Fail case
    ASSERT_EQ(m.MANAGER_FAIL, m.ite(a, 100, b));
}
/**
* \brief Test for ite compute table
*/
TEST(bdd, ite_ComputeTable)
{
    manager_t m;

    BDD_ID a = m.createVar("a");
    BDD_ID b = m.createVar("b");
    BDD_ID c = m.createVar("c");

    // New node added for this expression as it's not terminal case
    BDD_ID ab_or = m.or2(a, b);
    ASSERT_EQ(ab_or, m.or2(a, b));
    // already exists
    ASSERT_EQ(ab_or, m.ite(a, 1, b));   // Same as or2 above
    ASSERT_EQ(ab_or, m.ite(a, a, b));
    // add new one
    BDD_ID bc_and = m.and2(b, c);
    ASSERT_EQ(bc_and, m.and2(b, c));
    
    // Add one in the middle
    BDD_ID f = m.and2(ab_or, bc_and);
    ASSERT_EQ(f, m.and2(ab_or, bc_and));
}
/**
* \brief Cofactoring - no variable
*/
TEST(bdd, coFactorTrue)
{
    manager_t m;

    BDD_ID a = m.createVar("a");
    BDD_ID b = m.createVar("b");
    BDD_ID c = m.createVar("c");

    // check fail
    ASSERT_EQ(m.MANAGER_FAIL, m.coFactorTrue(-10));
    // b is inserted by default highId = 1, lowId = 0
    ASSERT_EQ(1, m.coFactorTrue(b));
    // 0 is special case changed in constructor
    ASSERT_EQ(0, m.coFactorTrue(0));
}
TEST(bdd, coFactorFalse)
{
    manager_t m;

    BDD_ID a = m.createVar("a");
    BDD_ID b = m.createVar("b");
    BDD_ID c = m.createVar("c");

    // check fail
    ASSERT_EQ(m.MANAGER_FAIL, m.coFactorFalse(-10));
    // b is inserted by default highId = 1, lowId = 0
    ASSERT_EQ(0, m.coFactorFalse(b));
    // 1 is special case changed in constructor
    ASSERT_EQ(1, m.coFactorFalse(1));
}
/**
* \brief Cofactoring - variable
*/
TEST(bdd, coFactorTrue_x)
{
    manager_t m;
    
    BDD_ID a = m.createVar("a");
    BDD_ID b = m.createVar("b");
    BDD_ID c = m.createVar("c");

    ASSERT_EQ(m.MANAGER_FAIL, m.coFactorTrue(-10, a));
    // terminal cases
    ASSERT_EQ(0, m.coFactorTrue(0, b));
    ASSERT_EQ(b, m.coFactorTrue(b, a));
    ASSERT_EQ(1, m.coFactorTrue(a, a));

    BDD_ID root = m.ite(c, a, b);
    ASSERT_EQ(a, m.coFactorTrue(root, c));
}
TEST(bdd, coFactorFalse_x)
{
    manager_t m;

    BDD_ID a = m.createVar("a");
    BDD_ID b = m.createVar("b");
    BDD_ID c = m.createVar("c");

    ASSERT_EQ(m.MANAGER_FAIL, m.coFactorFalse(-10, a));
    // Terminal cases
    ASSERT_EQ(1, m.coFactorFalse(1, b));
    ASSERT_EQ(b, m.coFactorFalse(b, a));
    ASSERT_EQ(0, m.coFactorFalse(a, a));

    BDD_ID root = m.ite(c, a, b);
    EXPECT_EQ(b, m.coFactorFalse(root, c));
}
/**
* \brief Find nodes
*/
TEST(bdd, findNodes)
{
    manager_t m;

    BDD_ID a = m.createVar("a");
    BDD_ID b = m.createVar("b");
    BDD_ID c = m.createVar("c");

    std::set<BDD_ID> nodesOfRoot    = std::set<BDD_ID>();
    std::set<BDD_ID> empty          = std::set<BDD_ID>();
    std::set<BDD_ID> One            = std::set<BDD_ID>();
    One.insert(1);
    
    // 5 is root node of ite(b,1,c)
    int content[] = {0, 1, 5, 4};
    std::set<BDD_ID> complex = std::set<BDD_ID>(content, content + 4);
    BDD_ID root = m.ite(c, a, b);

    m.findNodes(-10, nodesOfRoot);
    ASSERT_EQ(empty, nodesOfRoot);
    m.findNodes(1, nodesOfRoot);
    ASSERT_EQ(One, nodesOfRoot);
    nodesOfRoot = std::set<BDD_ID>();
    m.findNodes(5, nodesOfRoot);
    ASSERT_EQ(complex, nodesOfRoot);
}
/**
* \brief Find vars
*/
TEST(bdd, findVars)
{
    manager_t m;

    BDD_ID a = m.createVar("a");
    BDD_ID b = m.createVar("b");
    BDD_ID c = m.createVar("c");

    std::set<BDD_ID> varsOfRoot = std::set<BDD_ID>();
    const std::set<BDD_ID> empty = std::set<BDD_ID>();
    std::set<BDD_ID> One = std::set<BDD_ID>();
    One.insert(1);
    // 5 is root node of ite(b,1,c)
    int content[] = {0, 1, 2, 4};
    std::set<BDD_ID> complex = std::set<BDD_ID>(content, content + 4);
    BDD_ID root = m.ite(a, c, 1);

    m.findVars(-10, varsOfRoot);
    ASSERT_EQ(empty, varsOfRoot);

    m.findVars(1, varsOfRoot);
    ASSERT_EQ(One, varsOfRoot);

    varsOfRoot = std::set<BDD_ID>();
    m.findVars(root, varsOfRoot);
    ASSERT_EQ(complex, varsOfRoot);
}
//--------------------------------------------------
//boolean function
TEST(neg, retNegNot)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  ASSERT_EQ(m.MANAGER_FAIL, m.neg(-20));

  ASSERT_EQ(0, m.neg(1));

  BDD_ID notB = m.neg(b);
  ASSERT_EQ(0, m.coFactorTrue(notB));
  ASSERT_EQ(1, m.coFactorFalse(notB));

  //the BDD for a and b
  BDD_ID AandB = m.ite(a, b, 0);
  BDD_ID notAandB = m.ite(a, notB, 1);
  ASSERT_EQ(notAandB, m.neg(AandB));
}
//----------------------------------------------
//and's functions
TEST(and2, Constants)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  ASSERT_EQ(m.MANAGER_FAIL, m.and2(-20, 1));

  ASSERT_EQ(0, m.and2(1, 0));
  ASSERT_EQ(1, m.and2(1, 1));

  ASSERT_EQ(0, m.and2(0, a));
}

TEST(and2, assoitiv)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  BDD_ID AandB = m.and2(a, b);
  BDD_ID BandA = m.and2(b, a);
  ASSERT_EQ(AandB, BandA);
  ASSERT_EQ(a, m.and2(1, a));

  BDD_ID notB = m.neg(b);
  ASSERT_EQ(0, m.and2(b, notB));
}

TEST(and2, distributive)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");
  const BDD_ID c = m.createVar("c");

  BDD_ID AorB = m.ite(a, 1, b);
  BDD_ID AorC = m.ite(a, 1, c);
  BDD_ID BandC = m.ite(b, c, 0);
  BDD_ID erg = m.ite(a, 1, BandC);

  ASSERT_EQ(erg, m.and2(AorB, AorC));
}

TEST(nand2, Constants)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  ASSERT_EQ(m.MANAGER_FAIL, m.nand2(-20, 1));

  ASSERT_EQ(1, m.nand2(1, 0));
  ASSERT_EQ(0, m.nand2(1, 1));

  ASSERT_EQ(m.ite(a, 0, 1), m.nand2(1, a));
  ASSERT_EQ(1, m.nand2(0, a));
}

TEST(nand2, assoitiv)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  BDD_ID AnandB = m.nand2(a, b);
  BDD_ID BnandA = m.nand2(b, a);
  ASSERT_EQ(AnandB, BnandA);

  BDD_ID notB = m.neg(b);
  ASSERT_EQ(1, m.nand2(b, notB));
}

TEST(nand2, distributive)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");
  const BDD_ID c = m.createVar("c");

  BDD_ID AorB = m.ite(a, 1, b);
  BDD_ID AorC = m.ite(a, 1, c);
  BDD_ID negA = m.ite(a, 0, 1);
  BDD_ID negB = m.ite(b, 0, 1);
  BDD_ID negC = m.ite(c, 0, 1);

  BDD_ID erg = m.and2(negA, m.ite(negB, 1, negC));

  ASSERT_EQ(erg, m.nand2(AorB, AorC));
}

TEST(nand2, deMorgan)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  BDD_ID notA = m.neg(a);
  BDD_ID notB = m.neg(b);
  ASSERT_EQ(m.or2(notA, notB), m.nand2(a, b));
}
//----------------------------------------------------------
//or's functions
TEST(or2, Constants)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  ASSERT_EQ(m.MANAGER_FAIL, m.or2(-20, 1));

  ASSERT_EQ(1, m.or2(1, 0));
  ASSERT_EQ(1, m.or2(1, 1));
  ASSERT_EQ(0, m.or2(0, 0));

  ASSERT_EQ(a, m.or2(0, a));
}

TEST(or2, assoitiv)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  BDD_ID AorB = m.or2(a, b);
  BDD_ID BorA = m.or2(b, a);
  ASSERT_EQ(AorB, BorA);
  ASSERT_EQ(1, m.or2(1, a));

  BDD_ID notB = m.neg(b);
  ASSERT_EQ(1, m.or2(b, notB));
}

TEST(xor2, Constants)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  ASSERT_EQ(m.MANAGER_FAIL, m.xor2(-20, 1));

  ASSERT_EQ(1, m.xor2(1, 0));
  ASSERT_EQ(0, m.xor2(1, 1));
  ASSERT_EQ(0, m.xor2(0, 0));

  ASSERT_EQ(a, m.xor2(0, a));
}

TEST(xor2, assoitiv)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  BDD_ID AxorB = m.xor2(a, b);
  BDD_ID BxorA = m.xor2(b, a);
  ASSERT_EQ(AxorB, BxorA);

  BDD_ID notA = m.neg(a);
  ASSERT_EQ(notA, m.xor2(1, a));

  BDD_ID notB = m.neg(b);
  ASSERT_EQ(1, m.xor2(b, notB));
}

TEST(nor2, Constants)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  ASSERT_EQ(m.MANAGER_FAIL, m.nor2(-20, 1));

  ASSERT_EQ(0, m.nor2(1, 0));
  ASSERT_EQ(0, m.nor2(1, 1));
  ASSERT_EQ(1, m.nor2(0, 0));

  BDD_ID notA = m.neg(a);
  ASSERT_EQ(notA, m.nor2(0, a));
}

TEST(nor2, assoitiv)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  BDD_ID AnorB = m.nor2(a, b);
  BDD_ID BnorA = m.nor2(b, a);
  ASSERT_EQ(AnorB, BnorA);

  ASSERT_EQ(0, m.nor2(1, a));

  BDD_ID notB = m.neg(b);
  ASSERT_EQ(0, m.nor2(b, notB));
}

TEST(nor2, deMorgan)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");

  BDD_ID notA = m.neg(a);
  BDD_ID notB = m.neg(b);
  ASSERT_EQ(m.and2(notA, notB), m.nor2(a, b));
}

TEST(getTopVarName, reString)
{
  manager_t m;

  const BDD_ID a = m.createVar("a");
  const BDD_ID b = m.createVar("b");
  const BDD_ID c = m.createVar("c");

  ASSERT_EQ(std::to_string(m.MANAGER_FAIL), m.getTopVarName(-2));
  ASSERT_EQ("a", m.getTopVarName(a));

  BDD_ID root = m.ite(c, b, 0);
  ASSERT_EQ("b", m.getTopVarName(root));
}
#endif /* __TEST_SUITE_H__ */
